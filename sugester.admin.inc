<?php
/**
 * @file
 * Settings file.
 */

/**
 * Settings form.
 *
 * @return
 *   Form array.
 */
function sugester_settings_form() {
  $form = array();
  $form['sugester_id'] = array(
    '#default_value' => variable_get('sugester_id', ''),
    '#type' => 'textfield',
    '#title' => t('Sugester ID'),
    '#description' => t('Get id from www.sugester.pl'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

